﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HierarchicalNav
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void OnBlah1Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Blah1Page("I like to blah"));
        }

        async void OnBlah2Tapped(object sender, System.EventArgs e)
        {
            bool usersResponse = await DisplayAlert("Warning",
                         "Are you sure you would like to continue?",
                         "Yes",
                         "No");
            if (usersResponse == true)
            {
                await Navigation.PushAsync(new Blah2Page());
            }
        }

    }
}
