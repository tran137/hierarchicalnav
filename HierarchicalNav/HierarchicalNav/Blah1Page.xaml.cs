﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchicalNav
{
	public partial class Blah1Page : ContentPage
	{
		public Blah1Page (string something)
		{
			InitializeComponent ();
            MainLabel.Text = something;
		}
	}
}